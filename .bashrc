# ~/.bashrc: executed by bash(1) for non-login  config/bash.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac


# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# Enable some Bash 4 features when possible:
# * Check the window size after each command, if necessary recalculate LINES and COLUMNS.
# * Case-insensitive globbing (used in pathname expansion)
# * Autocorrect typos in path names when using `cd`
# * Autocd, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in checkwinsize nocaseglob cdspell autocd globstar; do
	shopt -s "$option" 2> /dev/null;
done;

# Prefer GB English and use UTF-8.
export LANG='en_GB.UTF-8';
export LC_ALL='en_GB.UTF-8';

# Make vim the default editor.
export EDITOR='vim';

# Highlight section titles in manual pages.
export LESS_TERMCAP_md="${yellow}";

# Don't clear the screen after quitting a manual page.
export MANPAGER='less -X';
# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# load all the other config files

src="$HOME/.config/bash"
source "$src/color"
source "$src/path"
source "$src/aliases"
source "$src/completion"
source "$src/history"
source "$src/prompt"
unset src
